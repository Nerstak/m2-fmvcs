package models;


import lombok.Getter;
import lombok.Setter;
import models.expressions.CTL;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import utils.JSONParsing;

import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.List;

import static utils.JSONParsing.getOrThrow;

/**
 * State has a name, labels (a non-present label is the equivalent of "NOT label"),
 * number of successors and seenBefore flag (modified by markings), CTL that have been checked in this state
 */
@Getter
public class State {
    private final String name;
    private final List<String> labels;
    @Setter
    private int numberOfSuccessors = 0;
    private HashMap<CTL, Boolean> checkedCTL;
    @Setter
    private boolean seenBefore;

    /**
     * Build from JSON
     * @param jsonObject JSON
     * @param atomicPropositions Atomic propositions in kripke
     */
    public State(JSONObject jsonObject, List<String> atomicPropositions) {
        name = (String) getOrThrow(jsonObject, "name");
        labels = JSONParsing.parseListString((JSONArray) getOrThrow(jsonObject, "labels"));
        checkedCTL = new HashMap<>();

        if(!atomicPropositions.containsAll(labels)) {
            throw new InputMismatchException("One or many labels are not in the set of atomic propositions");
        }
    }

    @Override
    public String toString() {
        return "State{" +
                "name='" + name + '\'' +
                ", labels=" + labels +
                ", numberOfSuccessors=" + numberOfSuccessors +
                ", validCtls=" + checkedCTL +
                ", seenBefore=" + seenBefore +
                '}';
    }

    /**
     * Reset the state, and its non-fundamental attributes
     */
    public void reset() {
        numberOfSuccessors = 0;
        checkedCTL = new HashMap<>();
        seenBefore = false;
    }
}

