package models.expressions;

import models.Kripke;
import models.State;
import models.Transition;

import java.util.ArrayList;
import java.util.List;

/**
 * A(leftField U rightField)
 */
public class AllUntil extends DoubleField {
    public AllUntil(CTL leftField, CTL rightField) {
        super(leftField, rightField);
    }

    public void marking(Kripke kripke) {
        super.marking(kripke);

        // Setting nb of successors
        kripke.getStates().forEach(state -> {
            state.setNumberOfSuccessors(Transition.findStartingAt(kripke.getTransitions(), state).size());
            state.getCheckedCTL().put(this, false);
        });

        // Finding first validate states
        List<State> validStates = new ArrayList<>();
        kripke.getStates().forEach(state -> {
            if (state.getCheckedCTL().get(rightField)) {
                validStates.add(state);
            }
        });

        // Checking parents of each valid state
        while (!validStates.isEmpty()) {
            State toRemove = validStates.remove(validStates.size() - 1);
            toRemove.getCheckedCTL().put(this, true);

            Transition.findEndingAt(kripke.getTransitions(), toRemove).forEach(transition -> {
                transition.getFrom().setNumberOfSuccessors(transition.getFrom().getNumberOfSuccessors() - 1);

                if (transition.getFrom().getNumberOfSuccessors() == 0 && transition.getFrom().getCheckedCTL().get(leftField) && !transition.getFrom().getCheckedCTL().get(this)){
                    validStates.add(transition.getFrom());
                }
            });
        }
    }
}
