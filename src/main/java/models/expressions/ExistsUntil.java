package models.expressions;

import models.Kripke;
import models.State;
import models.Transition;

import java.util.ArrayList;
import java.util.List;

/**
 * E(leftField U rightField)
 */
public class ExistsUntil extends DoubleField {
    public ExistsUntil(CTL leftField, CTL rightField) {
        super(leftField, rightField);
    }

    public void marking(Kripke kripke) {
        super.marking(kripke);

        kripke.getStates().forEach((state -> {
            state.getCheckedCTL().put(this, false);
            state.setSeenBefore(false);
        }));

        List<State> validStates = new ArrayList<>();
        kripke.getStates().forEach(state -> {
            if (state.getCheckedCTL().get(rightField)) {
                validStates.add(state);
            }
        });

        while (!validStates.isEmpty()) {
            State toRemove = validStates.remove(validStates.size() - 1);
            toRemove.getCheckedCTL().put(this, true);
            Transition.findEndingAt(kripke.getTransitions(), toRemove).forEach(transition -> {
                if (!transition.getFrom().isSeenBefore()) {
                    transition.getFrom().setSeenBefore(true);
                    if (transition.getFrom().getCheckedCTL().get(leftField)) {
                        validStates.add(transition.getFrom());
                    }
                }
            });
        }
    }
}