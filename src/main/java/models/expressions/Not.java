package models.expressions;

import models.Kripke;

/**
 * -field
 */
public class Not extends UniqueField {

    public Not(CTL field) {
        super(field);
    }

    public void marking(Kripke kripke) {
        super.marking(kripke);

        kripke.getStates().forEach((state ->
                state.getCheckedCTL().put(this, !state.getCheckedCTL().get((this).getField()))
        ));
    }
}
