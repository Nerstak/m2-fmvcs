package models.expressions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import models.Kripke;

@AllArgsConstructor
public class AtomicProposition implements CTL {
    @Getter
    private String proposition;

    @Override
    public String toString() {
        return "Expression{" +
                "proposition='" + proposition + '\'' +
                '}';
    }

    public void marking(Kripke kripke) {
        kripke.getStates().forEach((state -> {
            if (state.getLabels().contains(this.getProposition())){
                state.getCheckedCTL().put(this, true);
            } else {
                state.getCheckedCTL().put(this, false);
            }
        }));
    }
}
