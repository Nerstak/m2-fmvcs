package models.expressions;

import models.Kripke;

import java.util.List;

/**
 * fields[0] & fields[1] & ... & fields[n]
 */
public class And extends MultipleFields {
    public And(List<CTL> fields) {
        super(fields);
    }

    public void marking(Kripke kripke) {
        super.marking(kripke);

        kripke.getStates().forEach(state -> {
            // We store the boolean status within an array because we need to reach it from the lambda within the foreach
            // True by default, because one invalid field can invalidate the whole expression
            final boolean[] status = {true};
            fields.forEach(field -> {
                status[0] = status[0] & state.getCheckedCTL().get(field);
            });
            state.getCheckedCTL().put(this, status[0]);
        });
    }
}
