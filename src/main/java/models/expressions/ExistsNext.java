package models.expressions;

import models.Kripke;

/**
 * EX field
 */
public class ExistsNext extends UniqueField {

    public ExistsNext(CTL field) {
        super(field);
    }

    public void marking(Kripke kripke) {
        super.marking(kripke);

        kripke.getStates().forEach(state -> state.getCheckedCTL().put(this, false));

        kripke.getTransitions().forEach(transition -> {
            if (transition.getTo().getCheckedCTL().get(field)) {
                transition.getFrom().getCheckedCTL().put(this, true);
            }
        });
    }
}
