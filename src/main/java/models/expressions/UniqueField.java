package models.expressions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import models.Kripke;

/**
 * Holding a unique operand
 */
@AllArgsConstructor
public abstract class UniqueField implements CTL {

    @Getter
    protected CTL field;

    @Override
    public void marking(Kripke kripke) {
        field.marking(kripke);
    }
}
