package models.expressions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import models.Kripke;

import java.util.List;

/**
 * Holding multiple operands that will be concerned by the same operation
 */
@AllArgsConstructor
public abstract class MultipleFields implements CTL {
    @Getter
    protected List<CTL> fields;

    @Override
    public void marking(Kripke kripke) {
        fields.forEach(field -> field.marking(kripke));
    }
}
