package models.expressions;

import models.Kripke;

/**
 * true
 */
public class True implements CTL {

    @Override
    public void marking(Kripke kripke) {
        kripke.getStates().forEach(state -> state.getCheckedCTL().put(this, true));
    }
}
