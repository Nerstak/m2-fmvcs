package models.expressions;

import models.Kripke;

public interface CTL {
    /**
     * Mark children CTL, then current CTL
     * @param kripke Kripke against which the CTL is checked
     */
    void marking(Kripke kripke);
}
