package models.expressions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import models.Kripke;

/**
 * Holding two operands (whose order matter) that will be concerned by the same operation
 */
@AllArgsConstructor
public abstract class DoubleField implements CTL {

    @Getter
    protected CTL leftField;

    @Getter
    protected CTL rightField;

    @Override
    public void marking(Kripke kripke) {
        leftField.marking(kripke);
        rightField.marking(kripke);
    }
}
