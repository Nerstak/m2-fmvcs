package models;

import lombok.Getter;
import models.expressions.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import utils.JSONParsing;

import java.util.ArrayList;
import java.util.List;

import static utils.JSONParsing.getOrThrow;

/**
 * Kripke structure, composed of atomic propositions, states and transitions
 */
@Getter
public class Kripke {
    private final List<String> atomicPropositions;
    private final List<State> states = new ArrayList<>();
    private final List<Transition> transitions = new ArrayList<>();

    /**
     * Build from JSON
     * @param jsonObject JSON
     */
    public Kripke(JSONObject jsonObject) {
        // AP
        atomicPropositions = JSONParsing.parseListString((JSONArray) getOrThrow(jsonObject, "atomicPropositions"));

        // States
        JSONArray jsonStates = (JSONArray) getOrThrow(jsonObject, "states");
        for(Object item: jsonStates) {
            states.add(new State((JSONObject) item, atomicPropositions));
        }

        // Transitions
        JSONArray jsonTransitions = (JSONArray) getOrThrow(jsonObject, "transitions");
        for(Object item: jsonTransitions) {
            transitions.add(new Transition((JSONObject) item, states));
        }

        // NB of successors of each states
        for (State state: states) {
            state.setNumberOfSuccessors(Transition.findStartingAt(transitions, state).size());
        }
    }

    /**
     * Get initial state
     * @return Initial state
     */
    public State getInitialState() {
        // Note : we can proceed this way, because ArrayList is a sequential list
        if(states.size() != 0) {
            return states.get(0);
        }
        return null;
    }

    /**
     * Check if a CTL is valid for the Kripke
     * @param ctl CTL to check
     * @return Validity of CTL
     */
    public boolean validateCTL(CTL ctl) {
        ctl.marking(this);
        boolean validation = getInitialState().getCheckedCTL().get(ctl);
        states.forEach(State::reset);
        return validation;
    }


    @Override
    public String toString() {
        return "Kripke{" +
                "atomicPropositions=" + atomicPropositions +
                ", states=" + states +
                ", transitions=" + transitions +
                '}';
    }
}
