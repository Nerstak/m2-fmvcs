package models;

import lombok.Getter;
import org.json.simple.JSONObject;

import java.util.List;
import java.util.stream.Collectors;

import static utils.JSONParsing.getOrThrow;

/**
 * Transition is modeled as : StateFrom -> StateTo
 */
@Getter
public class Transition {
    private final State from;
    private final State to;

    /**
     * Build from JSON
     * @param jsonObject JSON
     * @param stateList List of states in kripke
     */
    public Transition(JSONObject jsonObject, List<State> stateList) {
        String fromName = (String) getOrThrow((JSONObject) getOrThrow(jsonObject, "from"), "name");
        String toName = (String) getOrThrow((JSONObject) getOrThrow(jsonObject, "to"), "name");

        from = stateList.stream().filter(state -> state.getName().equals(fromName)).findFirst().orElseThrow();
        to = stateList.stream().filter(state -> state.getName().equals(toName)).findFirst().orElseThrow();
    }

    /**
     * Find transition ending at a specific state <br>
     * STATE IS VERIFIED BY IDENTITY, NOT EQUALITY
     * @param transitions List of transition to test
     * @param state State to find
     * @return List of transition (maybe empty)
     */
    public static List<Transition> findEndingAt(List<Transition> transitions, State state) {
        return transitions.stream().filter(t -> t.to == state).collect(Collectors.toList());
    }

    /**
     * Find transition starting at a specific state <br>
     * STATE IS VERIFIED BY IDENTITY, NOT EQUALITY
     * @param transitions List of transition to test
     * @param state State to find
     * @return List of transition (maybe empty)
     */
    public static List<Transition> findStartingAt(List<Transition> transitions, State state) {
        return transitions.stream().filter(t -> t.from == state).collect(Collectors.toList());
    }

    @Override
    public String toString() {
        return "Transition{" +
                "from=" + from +
                ", to=" + to +
                '}';
    }
}

