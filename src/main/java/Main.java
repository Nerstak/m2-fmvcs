import models.Kripke;

import models.expressions.CTL;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import parser.ExpressionParsing;
import utils.SimpleFileReader;

import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;


public class Main {
    public static void main (String[] args) throws IOException, ParseException {
        if(args.length < 2) {
            throw new IllegalArgumentException("Argument structure is: [kripkeFileName.json placed inside kripke folder] [ctlFileName.txt placed inside ctl folder]");
        }

        JSONParser parser = new JSONParser();
        JSONObject jsonKripke = (JSONObject) parser.parse(new FileReader("kripke/" + args[0]));

        Kripke kripke = new Kripke(jsonKripke);
        ExpressionParsing ep = new ExpressionParsing(kripke);

        SimpleFileReader.readFile("ctl/" + args[1]).forEach(line -> {
            System.out.println(line);
            try {
                CTL ctl = ep.parse(line);
                System.out.println("   " + kripke.validateCTL(ctl));
            } catch (IOException|RuntimeException e) {
                System.out.println(e.getMessage());
            }
        });
    }
}
