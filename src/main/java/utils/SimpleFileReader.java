package utils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class SimpleFileReader {
    /**
     * Read File
     * @param filename Filename with path
     * @return Lines in list
     * @throws IOException Error during reading
     */
    public static List<String> readFile(String filename) throws IOException {
        return Files.lines(Paths.get(filename)).collect(Collectors.toList());
    }
}
