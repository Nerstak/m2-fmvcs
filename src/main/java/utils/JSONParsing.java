package utils;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;

/**
 * Utils function for JSON parsing
 */
public class JSONParsing {
    /**
     * Parse a JSON list of string to List of string in Java
     * @param array JSON array of string
     * @return Java List of string
     */
    public static List<String> parseListString(JSONArray array) {
        List<String> list = new ArrayList<>();

        for(Object item: array) {
            list.add((String) item);
        }

        return list;
    }

    /**
     * Get object from JsonObject, or throw exception if not existing
     * @param object JsonObject
     * @param key Key to get value
     * @return Java Object
     */
    public static Object getOrThrow(JSONObject object, String key) throws InputMismatchException {
        Object res = object.get(key);
        if (res == null) throw new InputMismatchException("Could not find key: " + key);
        return res;
    }
}
