package parser;

import parser.lexicon.LogicalOperators;
import parser.lexicon.Parenthesis;
import parser.lexicon.TemporalOperators;

import java.util.List;
import java.util.Objects;

/**
 * Type of Token
 */
public enum TokenType {
    PROPOSITION,
    EQUAL,
    LOGICAL_OPERATOR,
    VARIABLE,
    PARENTHESIS,
    TEMPORAL_OPERATOR;

    /**
     * Identify lexical type from a String
     * @param s String
     * @param atomicPropositions List of current atomic propositions
     * @return Type OR null
     */
    public static TokenType fromString(String s, List<String> atomicPropositions) {
        if(Objects.equals(LogicalOperators.fromString(s), LogicalOperators.EQUAL)) {
            // Equal is a special operator, as it does not have an impact on the result formula
            return EQUAL;
        } else if (LogicalOperators.contains(s)) {
            return LOGICAL_OPERATOR;
        } else if (TemporalOperators.contains(s)) {
            return TEMPORAL_OPERATOR;
        } else if (Parenthesis.contains(s)) {
            return PARENTHESIS;
        } else if (atomicPropositions.contains(s)) {
            return PROPOSITION;
        } else if (s.equals(s.toLowerCase())) {
            return VARIABLE;
        }

        return null;
    }
}
