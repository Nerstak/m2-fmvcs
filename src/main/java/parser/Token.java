package parser;

/**
 * Token with value and type (its meaning in an expression)
 */
public record Token(String value, TokenType type) {
}
