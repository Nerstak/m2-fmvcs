package parser;

import lombok.AllArgsConstructor;
import models.Kripke;

import java.io.IOException;
import java.io.StreamTokenizer;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;

/**
 * Tokenize and lexer of an expression, giving meaning to the tokens
 */
@AllArgsConstructor
public class ExpressionLexer {
    private Kripke kripke;
    /**
     * Convert a string (an expression) to a list of tokens, that can then be processed
     * @param s Expression
     * @return Stream of token
     * @throws IOException Error during tokenization
     */
    public List<Token> tokenize(String s) throws IOException, InputMismatchException {
        StreamTokenizer tokenizer = new StreamTokenizer(new StringReader(s));
        tokenizer.ordinaryChar('-');  // Minus is going to be considered an operator, not a number

        List<Token> streamOfTokens = new ArrayList<>();

        while (tokenizer.nextToken() != StreamTokenizer.TT_EOF) {
            switch (tokenizer.ttype) {
                case StreamTokenizer.TT_NUMBER -> streamOfTokens.add(lexicalAnalysis(String.valueOf(tokenizer.sval)));
                case StreamTokenizer.TT_WORD -> streamOfTokens.add(lexicalAnalysis(tokenizer.sval));
                default ->  // operator
                        streamOfTokens.add(lexicalAnalysis(String.valueOf((char) tokenizer.ttype)));
            }
        }
        return streamOfTokens;
    }

    /**
     * Perform a lexical analysis on the input, and identify its type
     * @param s Input
     * @return Token
     * @throws InputMismatchException In case type could not be identified
     */
    private Token lexicalAnalysis(String s) throws InputMismatchException {
        TokenType type = TokenType.fromString(s, kripke.getAtomicPropositions());
        if (type == null) {
            throw new InputMismatchException("Could not identify type of " + s);
        }
        return new Token(s, type);
    }
}
