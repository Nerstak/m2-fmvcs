package parser.lexicon;

/**
 * Operators of the lexicon
 */
public enum LogicalOperators {
    EQUAL("="),
    NEGATION("-"),
    OR("|"),
    AND("&");

    private final String value;
    LogicalOperators(String value) {
        this.value = value;
    }

    /**
     * Check if a given string is contained within the Enum
     * @param s String to test
     * @return Boolean
     */
    public static boolean contains(String s) {
        return fromString(s) != null;
    }

    /**
     * Identify operator from string
     * @param s String
     * @return Operator or null
     */
    public static LogicalOperators fromString(String s) {
        for (LogicalOperators op: LogicalOperators.values()) {
            if(op.value.equals(s)) {
                return op;
            }
        }
        return null;
    }
}
