package parser.lexicon;

/**
 * Temporal Operator of the Lexicon <br/>
 * Eg: AX is ONE operator, not A and X <br/>
 * Eg: A(...U...) is TWO operator, A and U
 */
public enum TemporalOperators {
    TRUE("true"),
    EXISTS("E"),
    ALL("A"),
    ALL_FINALLY("AF"),
    EXISTS_FINALLY("EF"),
    EXISTS_NEXT("EX"),
    ALL_NEXT("AX"),
    ALL_GLOBALLY("AG"),
    EXISTS_GLOBALLY("EG"),
    UNTIL("U");

    private final String value;

    TemporalOperators(String value) {
        this.value = value;
    }

    /**
     * Check if a given string is contained within the Enum
     * @param s String to test
     * @return Boolean
     */
    public static boolean contains(String s) {
        return fromString(s) != null;
    }

    /**
     * Identify operator from string
     * @param s String
     * @return Operator or null
     */
    public static TemporalOperators fromString(String s) {
        for (TemporalOperators to: TemporalOperators.values()) {
            if(to.value.equals(s)) {
                return to;
            }
        }
        return null;
    }
}
