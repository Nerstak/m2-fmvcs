package parser.lexicon;

/**
 * Parenthesis in the lexicon
 */
public enum Parenthesis {
    LEFT("("),
    RIGHT(")");

    private final String value;
    Parenthesis(String value) {
        this.value = value;
    }

    /**
     * Check if a given string is contained within the Enum
     * @param s String to test
     * @return Boolean
     */
    public static boolean contains(String s) {
        return fromString(s) != null;
    }

    /**
     * Identify parenthesis from string
     * @param s String
     * @return Parenthesis or null
     */
    public static Parenthesis fromString(String s) {
        for (Parenthesis p: Parenthesis.values()) {
            if(p.value.equals(s)) {
                return p;
            }
        }
        return null;
    }
}
