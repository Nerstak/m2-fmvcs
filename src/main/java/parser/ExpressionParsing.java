package parser;

import lombok.AllArgsConstructor;
import lombok.Getter;
import models.Kripke;
import models.expressions.*;
import parser.lexicon.LogicalOperators;
import parser.lexicon.Parenthesis;
import parser.lexicon.TemporalOperators;

import java.io.IOException;
import java.util.*;

/**
 * The expression parser is a Recursive top-down parser <br/>
 * giving priority to either temporal, logical operators<br/>
 * variables, parenthesis, etc.                         <br/>
 *                                                      <br/>
 * Eg: E(a|c U b & c) | true                            <br/>
 *                                                      <br/>
 *                                                      <br/>
 *   	    |                                           <br/>
 *   true 	  	 E                                      <br/>
 *               U                                      <br/>
 *            |      &                                  <br/>
 *          a   c   b   c                               <br/>
 */
public class ExpressionParsing {
    private int index = 0;
    private List<Token> tokens;
    private final ExpressionLexer expressionLexer;
    private final HashMap<String, CTL> variables = new HashMap<>();


    public ExpressionParsing(Kripke kripke) {
        expressionLexer = new ExpressionLexer(kripke);
    }

    /**
     * Get the value of the token of the current index
     * @return Value, or null if the index is not within range
     */
    private String getValueAtCurrentIndex() {
        if (index < tokens.size() && index >= 0) return tokens.get(index).value();
        return null;
    }

    /**
     * Get the type of the token of the current index
     * @return Type, or null if the index is not within range
     */
    private TokenType getTypeAtCurrentIndex() {
        if (index < tokens.size() && index >= 0) return tokens.get(index).type();
        return null;
    }

    /**
     * Parse a String CTL expression into a tree of CTLs
     * @param expression String of CTL
     * @return CTL
     * @throws IOException Error during process
     */
    public CTL parse(String expression) throws IOException {
        index = 0;

        // Extracting the actual formula from the global expression (removing variable assignement)
        List<Token> tokensWithVariableAssignment = expressionLexer.tokenize(expression);
        tokens = prepareVariableAssignment(tokensWithVariableAssignment);

        if(tokens.size() == 0) {
             throw new RuntimeException("Empty ctl");
        }

        CTL ctl = parseExpression();


        if(index < tokens.size()) {
            throw new RuntimeException("Could not process object after: " + tokens.get(index));
        }

        // Variable assignment
        if(tokens.size() != tokensWithVariableAssignment.size()) {
            variables.put(tokensWithVariableAssignment.get(0).value(), ctl);
        }
        return ctl;
    }

    /**
     * Prepare and identify a variable assignation
     * @param t Expression (with assignation)
     * @return Expression (without assignation)
     */
    private List<Token> prepareVariableAssignment(List<Token> t) {
        if(t.size() > 2 && t.get(1).type() == TokenType.EQUAL) {
            return t.subList(2, t.size());
        } else {
            return t;
        }
    }

    /**
     * Parse an expression containing an Until
     * @return CTL
     */
    private CTL parseUntil() {
        CTL ctl = parseExpression();
        if (TemporalOperators.fromString(getValueAtCurrentIndex()) == TemporalOperators.UNTIL) {
            index++;
            CTL a = ctl, b = parseExpression();
            ctl = new Until(a, b);
        }
        return ctl;
    }

    /**
     * Parse an expression that may contains an OR operator
     * @return CTL
     */
    private CTL parseExpression() {
        CTL ctl = parseTerm();
        while (true) {
            if (LogicalOperators.fromString(getValueAtCurrentIndex()) == LogicalOperators.OR) {
                index++;
                List<CTL> ctls = new ArrayList<>();
                if(ctl instanceof Or or) {
                    ctls.addAll(or.getFields());
                } else {
                    ctls.add(ctl);
                }
                ctls.add(parseTerm());

                ctl = new Or(ctls);
            } else {
                return ctl;
            }
        }
    }

    /**
     * Parse an expression that may contain an AND
     * @return CTL
     */
    private CTL parseTerm() {
        CTL ctl = parseFactor();
        while (true) {
            if (LogicalOperators.fromString(getValueAtCurrentIndex()) == LogicalOperators.AND) {
                index++;
                List<CTL> ctls = new ArrayList<>();
                if(ctl instanceof And and) {
                    ctls.addAll(and.getFields());
                } else {
                    ctls.add(ctl);
                }
                ctls.add(parseFactor());

                ctl = new And(ctls);
            } else {
                return ctl;
            }
        }
    }

    /**
     * Parse simple factor
     * @return CTL
     */
    private CTL parseFactor() {
        CTL ctl;
        if (Parenthesis.fromString(getValueAtCurrentIndex()) == Parenthesis.LEFT) {
            // Parenthesis
            index++;
            ctl = parseExpression();
            if (Parenthesis.fromString(getValueAtCurrentIndex()) == Parenthesis.RIGHT) {
                index++;
            } else {
                throw new RuntimeException("Missing parenthesis");
            }
        } else if (LogicalOperators.fromString(tokens.get(index).value()) == LogicalOperators.NEGATION) {
            // Negation operator
            index++;
            ctl = new Not(parseFactor());
        } else if (getTypeAtCurrentIndex() == TokenType.TEMPORAL_OPERATOR) {
            // Temporal operator
            TemporalOperators to = TemporalOperators.fromString(getValueAtCurrentIndex());
            index++;

            ctl = parseSimpleTemporalOperator(to);
        } else if (getTypeAtCurrentIndex() == TokenType.VARIABLE) {
            // Variables
            ctl = variables.get(getValueAtCurrentIndex());
            if (ctl == null) throw new RuntimeException("Variable " + getValueAtCurrentIndex() + " has not been assigned");
            index++;
        } else if (getTypeAtCurrentIndex() == TokenType.PROPOSITION) {
            // Proposition
            ctl = new AtomicProposition(getValueAtCurrentIndex());
            index++;
        } else {
            // Default
            throw new RuntimeException("Unexpected: " + getValueAtCurrentIndex());
        }

        return ctl;
    }

    /**
     * Parse a simple temporal operator (all operand are after operator)
     * @param to Operator
     * @return CTL
     */
    private CTL parseSimpleTemporalOperator(TemporalOperators to) {
        CTL ctl;
        ctl = switch (Objects.requireNonNull(to)) {
            case TRUE -> new True();
            case EXISTS -> buildExistsUntil(to);
            case ALL -> buildAllUntil(to);
            case EXISTS_NEXT -> new ExistsNext(parseFactor());
            // AX x <==> -EX -x
            case ALL_NEXT -> new Not(new ExistsNext(new Not(parseFactor())));
            // EF x <==> E(true U x)
            case EXISTS_FINALLY -> new ExistsUntil(new True(), parseFactor());
            // AF x <==> A(true U x)
            case ALL_FINALLY -> new AllUntil(new True(), parseFactor());
            // AG x <==> -EF(-x) <==> -E(true U -x)
            case ALL_GLOBALLY -> new Not(new ExistsUntil(new True(), new Not(parseFactor())));
            // EG x <==> -A -G x <==> -AF(-x) <==> -A(true U -x)
            case EXISTS_GLOBALLY -> new Not(new AllUntil(new True(), new Not(parseFactor())));
            default -> throw new RuntimeException("Unexpected value: " + Objects.requireNonNull(to));
        };
        return ctl;
    }

    /**
     * Build an Exists Until CTL
     * @param to Operator
     * @return Exists Until
     */
    private CTL buildExistsUntil(TemporalOperators to) {
        CTL tmp;
        if (Parenthesis.fromString(getValueAtCurrentIndex()) == Parenthesis.LEFT) {
            Until u = buildUntil(to);
            tmp =  new ExistsUntil(u.getLeft(), u.getRight());
        } else {
            throw new RuntimeException("Missing parenthesis after: " + Objects.requireNonNull(to));
        }
        return tmp;
    }

    /**
     * Build an All Until CTL
     * @param to Operator
     * @return All Until
     */
    private CTL buildAllUntil(TemporalOperators to) {
        CTL tmp;
        if (Parenthesis.fromString(getValueAtCurrentIndex()) == Parenthesis.LEFT) {
            Until u = buildUntil(to);
            tmp =  new AllUntil(u.getLeft(), u.getRight());
        } else {
            throw new RuntimeException("Missing parenthesis after: " + Objects.requireNonNull(to));
        }
        return tmp;
    }

    /**
     * Build an Until (temporary) CTL
     * @param to Operator
     * @return Until
     */
    private Until buildUntil(TemporalOperators to) {
        index++;
        CTL until = parseUntil();
        if (!(until instanceof Until u))
            throw new RuntimeException("Unexpected value: " + Objects.requireNonNull(to));
        if (Parenthesis.fromString(getValueAtCurrentIndex()) == Parenthesis.RIGHT) {
            index++;
            return u;
        } else {
            throw new RuntimeException("Missing parenthesis after: " + Objects.requireNonNull(to));
        }
    }

    /**
     * Until expression is here to prevent any external usage <br/>
     * It should only be used within the parsing
     */
    @Getter
    @AllArgsConstructor
    private static class Until implements CTL {
        private CTL left;
        private CTL right;

        @Override
        public void marking(Kripke kripke) {
            throw new RuntimeException("Until should not be used in marking");
        }
    }
}
