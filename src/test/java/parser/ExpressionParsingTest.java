package parser;

import models.Kripke;
import models.expressions.*;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.FileReader;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


class ExpressionParsingTest {
    ExpressionParsing expressionParsing;

    @BeforeEach
    void init() throws IOException, ParseException {
        JSONParser parser = new JSONParser();
        JSONObject jsonKripke = (JSONObject) parser.parse(new FileReader("kripke/example.json"));

        Kripke kripke = new Kripke(jsonKripke);
        expressionParsing = new ExpressionParsing(kripke);
    }

    @Test
    void testExpression() throws IOException {
        CTL ctl = expressionParsing.parse("a");

        assertTrue(ctl instanceof AtomicProposition, "Expression correctly created");
        assertEquals("a", ((AtomicProposition) ctl).getProposition(), "Correct expression");
    }

    @Test
    void testTrue() throws IOException {
        CTL ctl = expressionParsing.parse("true");

        assertTrue(ctl instanceof True, "True correctly created");
    }

    @Test
    void testNegation() throws IOException {
        CTL ctl = expressionParsing.parse("-a");

        assertTrue(ctl instanceof Not, "Not correctly created");
        assertTrue(((Not) ctl).getField() instanceof AtomicProposition, "Correct inner ctl");
    }

    @Test
    void testAnd() throws IOException {
        CTL ctl = expressionParsing.parse("a & b & c");

        assertTrue(ctl instanceof And, "And correctly created");
        assertEquals(3, ((And) ctl).getFields().size(), "Correct number of inner elements");
    }

    @Test
    void testOr() throws IOException {
        CTL ctl = expressionParsing.parse("a | b | c");

        assertTrue(ctl instanceof Or, "Or correctly created");
        assertEquals(3, ((Or) ctl).getFields().size(), "Correct number of inner elements");
    }

    @Test
    void testOrWithAnd() throws IOException {
        CTL ctl = expressionParsing.parse("a | b & c");

        assertTrue(ctl instanceof Or, "Or correctly created");
        assertEquals(2, ((Or) ctl).getFields().size(), "Correct number of inner elements");
        Or or = (Or) ctl;
        assertTrue(or.getFields().get(0) instanceof AtomicProposition, "Correct first operand");
        assertTrue(or.getFields().get(1) instanceof And, "Correct second operand");
    }

    @Test
    void testOrWithAndParenthesis() throws IOException {
        CTL ctl = expressionParsing.parse("(a | b) & c");

        assertTrue(ctl instanceof And, "And correctly created");
        assertEquals(2, ((And) ctl).getFields().size(), "Correct number of inner elements");
        And and = (And) ctl;
        assertTrue(and.getFields().get(0) instanceof Or, "Correct first operand");
        assertTrue(and.getFields().get(1) instanceof AtomicProposition, "Correct second operand");
    }

    @Test
    void testExistsUntil() throws IOException {
        CTL ctl = expressionParsing.parse("E(a U true)");

        assertTrue(ctl instanceof ExistsUntil, "ExistsUntil correctly created");
        ExistsUntil ex = (ExistsUntil) ctl;
        assertTrue(ex.getLeftField() instanceof AtomicProposition, "Inner Expression correctly created");
        assertTrue(ex.getRightField() instanceof True, "Inner True correctly created");
    }

    @Test
    void testAllUntil() throws IOException {
        CTL ctl = expressionParsing.parse("A(a U true)");

        assertTrue(ctl instanceof AllUntil, "AllUntil correctly created");
        AllUntil ax = (AllUntil) ctl;
        assertTrue(ax.getLeftField() instanceof AtomicProposition, "Inner Expression correctly created");
        assertTrue(ax.getRightField() instanceof True, "Inner True correctly created");
    }

    @Test
    void testUntilComplexInner() throws IOException {
        CTL ctl = expressionParsing.parse("A(a U -a | true)");
        assertTrue(ctl instanceof AllUntil, "AllUntil correctly created");

        AllUntil ax = (AllUntil) ctl;
        assertTrue(ax.getLeftField() instanceof AtomicProposition, "Inner Expression correctly created");
        assertTrue(ax.getRightField() instanceof Or, "Inner Or correctly created");

        Or or = (Or) ax.getRightField();
        assertEquals(2, or.getFields().size(), "Correct number of elements in or");
        assertTrue(or.getFields().get(0) instanceof Not, "Inner Not correctly created");
        assertTrue(or.getFields().get(1) instanceof True, "Inner True correctly created");
    }

    @Test
    void testExistsNext() throws IOException {
        CTL ctl = expressionParsing.parse("EX a");

        assertTrue(ctl instanceof ExistsNext, "ExistsNext correctly created");
        ExistsNext en = (ExistsNext) ctl;
        assertTrue(en.getField() instanceof AtomicProposition, "Inner Expression correctly created");
    }

    @Test
    void testAllNext() throws IOException {
        CTL ctl = expressionParsing.parse("AX a");

        assertTrue(ctl instanceof Not, "Not correctly created");
        assertTrue(((Not) ctl).getField() instanceof ExistsNext, "Inner ExistsNext correctly created");

        ExistsNext en = (ExistsNext) ((Not) ctl).getField();
        assertTrue(en.getField() instanceof Not, "Inner Not correctly created");
        assertTrue(((Not) en.getField()).getField() instanceof AtomicProposition, "Inner Expression correctly created");
    }

    @Test
    void testExistsFinally() throws IOException {
        CTL ctl = expressionParsing.parse("EF(a | true)");

        assertTrue(ctl instanceof ExistsUntil, "ExistsUntil correctly created");
        assertTrue(((ExistsUntil) ctl).getLeftField() instanceof True, "Inner True correctly created");
        assertTrue(((ExistsUntil) ctl).getRightField() instanceof Or, "Inner Or correctly created");
    }

    @Test
    void testAllFinally() throws IOException {
        CTL ctl = expressionParsing.parse("AF(b & c & EX a)");

        assertTrue(ctl instanceof AllUntil, "AllUntil correctly created");
        assertTrue(((AllUntil) ctl).getLeftField() instanceof True, "Inner True correctly created");
        assertTrue(((AllUntil) ctl).getRightField() instanceof And, "Inner And correctly created");
        assertEquals(3, ((And) ((AllUntil) ctl).getRightField()).getFields().size(), "Inner And has a correct size");
    }

    @Test
    void testAllGlobally() throws IOException {
        CTL ctl = expressionParsing.parse("AG(b | true)");

        assertTrue(ctl instanceof Not, "Not correctly created");
        assertTrue(((Not) ctl).getField() instanceof ExistsUntil, "ExistsUntil correctly created");

        ExistsUntil ex = (ExistsUntil) ((Not) ctl).getField();
        assertTrue(ex.getLeftField() instanceof True, "Inner True correctly created");
        assertTrue(ex.getRightField() instanceof Not, "Inner Or correctly created");

        Not not = (Not) ex.getRightField();
        assertTrue(not.getField() instanceof Or, "Inner Or correctly created");
        assertEquals(2, ((Or) not.getField()).getFields().size(), "Inner And has a correct size");
    }

    @Test
    void testExistsNextComplexInner() throws IOException {
        CTL ctl = expressionParsing.parse("EX(a | true & -b)");

        assertTrue(ctl instanceof ExistsNext, "ExistsNext correctly created");

        ExistsNext en = (ExistsNext) ctl;
        assertTrue(en.getField() instanceof Or, "Inner Or correctly created");

        Or or = (Or) en.getField();
        assertEquals(2, or.getFields().size(), "Correct or Size");
        assertTrue(or.getFields().get(0) instanceof AtomicProposition, "Inner Expression correctly created");
        assertTrue(or.getFields().get(1) instanceof And, "Inner And correctly created");

        And and = (And) or.getFields().get(1);
        assertEquals(2, and.getFields().size(), "Correct And Size");
        assertTrue(and.getFields().get(0) instanceof True, "Inner True correctly created");
        assertTrue(and.getFields().get(1) instanceof Not, "Inner Not correctly created");
        assertTrue(((Not) and.getFields().get(1)).getField() instanceof AtomicProposition, "Inner Expression correctly created");
    }

    @Test
    void testComplexExpression1() throws IOException {
        CTL ctl = expressionParsing.parse("EX(a | true & -b) | b");

        assertTrue(ctl instanceof Or, "Or correctly created");

        Or or = (Or) ctl;
        assertEquals(2, or.getFields().size(), "Correct or Size");
        assertTrue(or.getFields().get(0) instanceof ExistsNext, "Inner ExistsNext correctly created");
        assertTrue(or.getFields().get(1) instanceof AtomicProposition, "Inner Expression correctly created");
    }

    @Test
    void testComplexExpression2() throws IOException {
        CTL ctl = expressionParsing.parse("E(a U true & b) | -c");

        assertTrue(ctl instanceof Or, "Or correctly created");

        Or or = (Or) ctl;
        assertEquals(2, or.getFields().size(), "Correct OR Size");
        assertTrue(or.getFields().get(0) instanceof ExistsUntil, "Inner ExistsUntil correctly created");
        assertTrue(or.getFields().get(1) instanceof Not, "Inner Expression correctly created");
    }

    @Test
    void testVariables() throws IOException {
        CTL ctlInVar = expressionParsing.parse("var = E(a U true & b) | -c");

        CTL ctl = expressionParsing.parse("var & -true");
        assertTrue(ctl instanceof And, "And correctly created");
        assertEquals(2, ((And) ctl).getFields().size(), "Correct AND size");

        And and = (And) ctl;
        assertEquals(and.getFields().get(0), ctlInVar, "Variable replacement working");
        assertTrue(and.getFields().get(1) instanceof Not, "Not correctly created");
    }
}