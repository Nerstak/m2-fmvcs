# M2-FMVCS

## Table of contents

- [Informations](#Informations)

- [Usage](#Usage)

## Informations

### Description

This program provides a CTL checking on Kripke Structures.

### Global structure

This program is composed of different parts:
- A Kripke Structure (composed of States and Transitions), with JSON reading
- A Lexer (tokenization of inputs and given meaning to each token) and a Parser (conversion of tokens to a usable CTL object)
- Multiple CTL implementations, each containing inner CTLs, and having a recursive checking method (marking)

## Usage

### Running the program

You need to run the program with two arguments: [kripke file.json] [ctls file.txt]. Both files must be located in their respective folders.
Example: `java Main kripke0.json kripke0.txt`. 

### Writing a valid Kripke Structure

The Kripke File must be a json, located in the `kripke` folder. It must have the same structure as shown below. The first defined state is the initial state.

```
{
  "atomicPropositions": [
    "a",
    "b", 
    "c"
  ],
  "states": [
    {
      "name": "state0",
      "labels": [
        "a",
        "b"
      ]
    },
    {
      "name": "state1",
      "labels": [
        "a"
      ]
    }
  ],
  "transitions": [
    {
      "from": {
        "name": "state0"
      },
      "to": {
        "name": "state1"
      }
    }
  ]
}
```

### Writing a valid CTL file

The CTL file contains the list of CTL that will be checked against the Kripke Structure. Each line must contain only one CTL.

#### Lexicon

| Meaning            | Formal Writing | Writing in the program | Notes                                                                                                                                             |
|:------------------:|:--------------:|:----------------------:|:-------------------------------------------------------------------------------------------------------------------------------------------------:|
| Defined as         |     φ ≔ ψ1     |         φ = ψ1         | Variable assignement is only possible if the name of the variable is not an operator or a reserved keyword (true for eg) or an atomic proposition |
| Atomic proposition |       p        |           p            | `p` must be within the list of labels in the Kripke                                                                                               |
| true               |      true      |          true          |                                                                                                                                                   |
| Negation           |      ¬ ψ1      |          -ψ1           |                                                                                                                                                   |
| And                |    ψ1 ∧ ψ2     |        ψ1 & ψ2         |                                                                                                                                                   |
| Or                 |    ψ1 ∨ ψ2     |          ψ1 \          | ψ2               |                                                                                                                                                   |
| Parenthesis        |      (ψ1)      |          (ψ1)          | Parenthesis go by pairs, and an opening one must be followed by a closing one within the reachability scope                                       |
| All Until          |   A ψ1 U ψ2    |       A(ψ1 U ψ2)       | Parenthesis are mandatory for readability and pratical purposes                                                                                   |
| Exists Until       |   E ψ1 U ψ2    |       E(ψ1 U ψ2)       | Parenthesis are mandatory for readability and pratical purposes                                                                                   |
| All Next           |     AX ψ1      |         AX ψ1          |                                                                                                                                                   |
| Exists Next        |     EX ψ1      |         EX ψ1          |                                                                                                                                                   |
| All Globally       |     AG ψ1      |         AG ψ1          |                                                                                                                                                   |
| Exists Globally    |     EG ψ1      |         EG ψ1          |                                                                                                                                                   |
| All Finally        |     AF ψ1      |         AF ψ1          |                                                                                                                                                   |
| Exists Finally     |     EF ψ1      |         EF ψ1          |                                                                                                                                                   |

Notes: 

- Temporal operators must be capitalized in upper case

- Temporal operators, atomic propositions and variables must be separated by spaces in order to be processed

- Variables can be used in following CTL. An undefined variable used in a CTL will prevent any check of the CTL. 

#### Examples of valid CTLs

- ctl0 = true

- ctl1 = ctl0 & a

- ctl2 = AF -a

- ctl3 = EF(-a & b)

- ctl4 = A(a U b)

- ctl5 = E(a U b &          -c) | EX a & EX b

- AG a

- EG a
